CONFIG_NAME = "Model12"
BATCH_SIZE = 64
LEARNING_RATE = 0.0005
OPTIMIZER = tf.keras.optimizers.Nadam(learning_rate=LEARNING_RATE)

        self.loss_function = tf.keras.losses.CategoricalCrossentropy()
        self.optimizer = OPTIMIZER
        self.loss_metric = tf.keras.metrics.Mean(name='loss')
        self.accuracy_metric = tf.keras.metrics.CategoricalAccuracy(name='accuracy')

        self.conv_layer1 = tf.keras.layers.Conv2D(filters=32, kernel_size=3, padding='same', activation='relu')
        self.conv_layer2 = tf.keras.layers.Conv2D(filters=32, kernel_size=3, padding='same', activation='relu')
        self.pool_layer3 = tf.keras.layers.MaxPooling2D(pool_size=2)

        self.conv_layer4 = tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')
        self.conv_layer5 = tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')
        self.pool_layer6 = tf.keras.layers.MaxPooling2D(pool_size=2)

        self.conv_layer7 = tf.keras.layers.Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')
        self.conv_layer8 = tf.keras.layers.Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')
        self.pool_layer9 = tf.keras.layers.GlobalAveragePooling2D()

        self.out_layer10 = tf.keras.layers.Dense(10, 'softmax')