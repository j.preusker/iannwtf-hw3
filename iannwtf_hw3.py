# IANNwTF HW3

import tensorflow as tf
import tensorflow_datasets as tfds
import numpy as np
import matplotlib.pylab as plt
import tqdm


#CONSTANTS
CONFIG_NAME = "Model15"
BATCH_SIZE = 64
LEARNING_RATE = 0.0005
OPTIMIZER = tf.keras.optimizers.Nadam(learning_rate=LEARNING_RATE)


#FETCH AND PREPARE DATA
(train_ds, test_ds) = tfds.load('cifar10', split=['train', 'test'], as_supervised=True)


def prep_cifar10(img, target):
    #cast pixel values to float32
    img = tf.cast(img, tf.float32)
    #normalize pixel values to [0,1]
    img = img / 255
    #one-hot encode targets
    target = tf.one_hot(target, depth=10)
    return img, target


#VISUALIZING SOME SAMPLES
categories = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
training_data = train_ds.map(lambda img, target: prep_cifar10(img, target))
training_iter = training_data.__iter__()
for i, (im, target) in enumerate(training_iter):
    plot = plt.subplot2grid((3,5), (int(i/5), i%5))
    plot.set_title(categories[tf.argmax(target)])
    plot.imshow(im)
    if i == 14:
        break
plt.show()


#DEFINE CNN
class cifar10_conv(tf.keras.Model):
    def __init__(self):
        super().__init__()

        self.loss_function = tf.keras.losses.CategoricalCrossentropy()
        self.optimizer = OPTIMIZER
        self.loss_metric = tf.keras.metrics.Mean(name='loss')
        self.accuracy_metric = tf.keras.metrics.CategoricalAccuracy(name='accuracy')

        self.conv_layer1 = tf.keras.layers.Conv2D(filters=16, kernel_size=3, padding='same', activation='relu')
        self.conv_layer2 = tf.keras.layers.Conv2D(filters=16, kernel_size=3, padding='same', activation='relu')
        self.pool_layer3 = tf.keras.layers.MaxPooling2D(pool_size=2)

        self.conv_layer4 = tf.keras.layers.Conv2D(filters=32, kernel_size=3, padding='same', activation='relu')
        self.conv_layer5 = tf.keras.layers.Conv2D(filters=32, kernel_size=3, padding='same', activation='relu')
        self.pool_layer6 = tf.keras.layers.MaxPooling2D(pool_size=2)

        self.conv_layer7 = tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')
        self.conv_layer8 = tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')
        self.pool_layer9 = tf.keras.layers.GlobalAveragePooling2D()

        self.out_layer10 = tf.keras.layers.Dense(10, 'softmax')



    #@tf.function
    def call(self, x):
        x = self.conv_layer1(x)
        x = self.conv_layer2(x)
        x = self.pool_layer3(x)
        x = self.conv_layer4(x)
        x = self.conv_layer5(x)
        x = self.pool_layer6(x)
        x = self.conv_layer7(x)
        x = self.conv_layer8(x)
        x = self.pool_layer9(x)
        x = self.out_layer10(x)
        return x
    

    #@tf.function
    def train_step(self, input):    #train model on one batch, so input should have shape ((BATCH_SIZE, 32, 32, 3), (10))

        x, targets = input
        
        with tf.GradientTape() as tape:
            predictions = self(x, training=True)
            loss = self.loss_function(targets, predictions)
        gradients = tape.gradient(loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        #update metrics
        self.loss_metric.update_state(loss)
        self.accuracy_metric.update_state(targets, predictions)



    @tf.function
    def test_step(self, input):

        x, targets = input

        predictions = self(x, training=False)
        loss = self.loss_function(targets, predictions)

        self.loss_metric.update_state(loss)
        self.accuracy_metric.update_state(targets, predictions)

        return self.loss_metric.result(), self.accuracy_metric.result()
    

    def reset_metrics(self):
        for metric in self.metrics:
            metric.reset_states()
            
        
#TRAINING LOOP        
def train_loop(model, epochs, training_data, test_data, summary_writer_train, summary_writer_test):

    for epoch in range(1, epochs+1):
        print(f'start epoch {epoch}')

        for batch in tqdm.tqdm(training_data, position=0, leave=True):
            model.train_step(batch)

        with summary_writer_train.as_default():
            for metric in model.metrics:
                tf.summary.scalar(name=f'training {metric.name}', data=metric.result(), step=epoch)
                print(f'training {metric.name}: {metric.result()}')
        model.reset_metrics()


        for batch in tqdm.tqdm(test_data, position=0, leave=True):
            model.test_step(batch)

        with summary_writer_test.as_default():
            for metric in model.metrics:
                tf.summary.scalar(name=f'test {metric.name}', data=metric.result(), step=epoch)
                print(f'test {metric.name}: {metric.result()}')
        model.reset_metrics()




def main():
    training_data = train_ds.map(lambda img, target: prep_cifar10(img, target))
    training_data = training_data.shuffle(1000).batch(BATCH_SIZE).prefetch(20) 
    test_data = train_ds.map(lambda img, target: prep_cifar10(img, target))
    test_data = test_data.shuffle(1000).batch(BATCH_SIZE).prefetch(20)

    myModel = cifar10_conv()
    summary_writer_train = tf.summary.create_file_writer(f'logs/{CONFIG_NAME}/training', name=f'{CONFIG_NAME}_training', filename_suffix=f'{CONFIG_NAME}_training')
    summary_writer_test = tf.summary.create_file_writer(f'logs/{CONFIG_NAME}/test', name=f'{CONFIG_NAME}_test', filename_suffix=f'{CONFIG_NAME}_test')

    train_loop(myModel, 15, training_data, test_data, summary_writer_train, summary_writer_test)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("KeyboardInterrupt received")